lang = ['Python','Golang','C++','JavaScript','Ruby','Swift','C','Java','PHP','Rust'] #ex. 8.3

print(f'Os três primeiros itens da minha lista são {lang[:3]}')

print(f'Os três últimos itens da lista são {lang[-3:]}')

print(f'Os três itens do meio são {lang[3:6]}')