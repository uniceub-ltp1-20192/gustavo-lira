# encoding: utf-8
import random

def listInviteCreate():
    nomes.append(input('Convidado 1: ').title())
    nomes.append(input('Convidado 2: ').title())
    nomes.append(input('Convidado 3: ').title())

def dinnerInvite():
    print('-'*40)
    for i in nomes:
        print(f'Boa noite {i}! Gostaria de vir jantar?')
    print('-'*40)

def dinnerDenied():
    who = random.randint(0,2)
    print(f'{nomes[who]} não poderá ir ao jantar.')
    flop = nomes.pop(who)
    nomes.append(input('Novo Convidado: ').title())
    print(f'Vou convidar {nomes[-1]} no lugar de {flop}.')

def newTable():
    print('Consegui uma mesa maior!')
    nomes.insert(0, input('Convidado 4: ').title())
    nomes.insert(2, input('Convidado 5: ').title())
    nomes.append(input('Convidado 6: ').title())

nomes = []

listInviteCreate()
dinnerInvite()
dinnerDenied()
dinnerInvite()
newTable()
dinnerInvite()