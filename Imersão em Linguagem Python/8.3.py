lang = ['Python','Golang','C++','JavaScript','Ruby','Swift','C','Java','PHP','Rust']

print(lang)

lang.sort()
print(lang)

lang.sort(reverse=True)
print(lang)

print(sorted(lang))

lang.reverse()
print(lang)

print(len(lang))