def originalList():
    print('Lista Original: ')
    print(countries)
    print('')

def sortedList():
    print('Lista em ordem alfabética: ')
    print(sorted(countries))
    print('')

def reverseSortedList():
    print('Lista em ordem alfabética inversa:')
    print(sorted(countries, reverse=True))
    print('')

def reverseMethod():
    print('Invertendo tudo:')
    countries.reverse()
    print(countries)
    print('')

def sortMethod():
    print('Alterando para ordem alfabética:')
    countries.sort()
    print(countries)
    print('')

def sortReverseMethod():
    print('Alterando para ordem alfabética inversa:')
    countries.sort(reverse=True)
    print(countries)
    print('')


countries = ['Japão','Nova Zelândia','China','Alemanha','Reino Unido']

originalList()
sortedList()
originalList()
reverseSortedList()
originalList()
reverseMethod()
originalList()
reverseMethod()
originalList()
sortMethod()
originalList()
sortReverseMethod()
originalList()
