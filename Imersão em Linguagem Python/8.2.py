# encoding: utf-8
import random

def listInviteCreate():
    nomes.append(input('Convidado 1: ').title())
    nomes.append(input('Convidado 2: ').title())
    nomes.append(input('Convidado 3: ').title())
    atualNumber()

def dinnerInvite():
    print('-'*40)
    for i in nomes:
        print(f'Boa noite {i}! Gostaria de vir jantar?')
    print('-'*40)

def dinnerDenied():
    sorteio = random.randint(0, 2)
    print(f'{nomes[sorteio]} não poderá ir ao jantar.')
    flop = nomes.pop(sorteio)
    nomes.append(input('Novo Convidado: ').title())
    print(f'Vou convidar {nomes[-1]} no lugar de {flop}.')

def newTable():
    print('Consegui uma mesa maior!')
    atualNumber()
    nomes.insert(0, input('Convidado 4: ').title())
    nomes.insert(2, input('Convidado 5: ').title())
    nomes.append(input('Convidado 6: ').title())
    atualNumber()

def getOut():
    print('Minha mesa não chegou! Só poderei chamar 2 pessoas.')
    print(f'Sua lista: {nomes}')
    atualNumber()
    while len(nomes) > 2:
        try:
            azarado = int(input(f'Escolha quem você vai remover (1-{len(nomes)}): '))
            expulso = nomes.pop(azarado-1)
            print('')
            print(f'Desculpe {expulso}, não poderá mais vir.')
            atualNumber()
            print(f'Restantes: {nomes}')
        except:
            print('Tá errado')
    print('')
    for n in nomes:
        print(f'Olá {n}, ainda poderá vir!')
    del nomes[1]
    del nomes[0]
    print('')
    print(f'Lista atual: {nomes}')

def atualNumber():
    print('N° de convidados existentes: ',end='')
    print(len(nomes))


# Programa Principal

nomes = []

listInviteCreate()
dinnerInvite()
dinnerDenied()
dinnerInvite()
newTable()
dinnerInvite()
getOut()