import sys

age = int(input('Informe sua idade: '))
semestre = int(input('Informe seu semestre atual: '))
if semestre > 8:
    sys.exit('O seu curso só possui 8 semestres')

anos = (8 - semestre)/2

print(f'Você vai se formar em {anos} ano(s)')