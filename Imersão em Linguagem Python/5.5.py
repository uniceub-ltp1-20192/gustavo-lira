import sys

age = input('Informe sua idade: ')
semestre = int(input('Informe seu semestre atual: '))

if semestre > 8:
    sys.exit('O seu curso só possui 8 semestres')

atrasado = int(input('Quantos semestres está atrasado?: '))

if atrasado > semestre:
    sys.exit('????????')

formyear = 8 - semestre
anos = formyear / 2
total = anos + atrasado / 2

if atrasado > 0:
    print(f'Com o atraso, você vai se formar em {total} anos')
    sys.exit()

print(f'Você vai se formar em {anos} ano(s)')

