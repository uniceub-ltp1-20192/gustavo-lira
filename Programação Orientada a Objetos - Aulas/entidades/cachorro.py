class Cachorro:
    def __init__(self, tamanho=0.0, peso=0.0, cor=""):
        self._tamanho = tamanho
        self._peso = peso
        self._cor = cor

    @property  # método de get
    def tamanho(self):
        return self._tamanho

    @tamanho.setter  # método de set
    def tamanho(self, tamanho):
        self._tamanho = tamanho

    @property
    def peso(self):
        return self._peso

    @peso.setter
    def peso(self, peso):
        self._peso = peso

    @property
    def cor(self):
        return self._cor

    @cor.setter
    def cor(self, cor):
        self._cor = cor

    def latir(self):
        return 'oooof oooof'

    def comer(self):
        return 'args args'