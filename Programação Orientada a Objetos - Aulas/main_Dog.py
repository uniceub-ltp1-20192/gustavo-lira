from entidades.cachorro import Cachorro
from entidades.pug import Pug

v = Cachorro()
p = Pug()

v.tamanho = 3.5
v.peso = 4.7
v.cor = 'Caramelo'
p.rabo = 'Enrolado'

print(f'''O tamanho do cachorro é {v.tamanho} m
O peso do cachorro é {v.peso} kg
A cor do cachorro é {v.cor}

{v.latir()}
{v.comer()}

O rabo do pug é {p.rabo}
''')

print(vars(v))
print(vars(p))
