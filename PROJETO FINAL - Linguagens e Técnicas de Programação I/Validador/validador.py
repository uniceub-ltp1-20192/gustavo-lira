import re

class Validador: 
  @staticmethod
  def validar(expReg, msgInvalido):
      teste = False
      while teste == False:
          valor = input('>>> ')
          verificarEx = re.match(expReg,valor)
          if verificarEx == None:
              print(msgInvalido.format(expReg))
          else:
              return valor
      
  @staticmethod
  def validarValorEntrada(valorAtual,msg):
      novoValor = input(msg)
      if novoValor != None and novoValor != "":
          return novoValor
      return valorAtual