class Menu:
  
  @staticmethod
  def menuEmpresa():
    print('>>>>> GERENCIADOR DE ESTOQUE <<<<<')
    n = input('Nome da sua empresa: ').title()
    if n == '':
        n = 'Mercadinho'
    return n

  @staticmethod
  def menuPrincipal(cn):
    def title(text):
      print('*'*len(text)*2)
      print(text.center(len(text)*2,' '))
      print('*'*len(text)*2)
    
    title(cn)
    print('[1] Mostrar Lista de Produtos\n[2] Comprar\n[3] Vender\n[4] Adicionar Itens\n[5] Sair')