lista = []
soma = 0

while True:
    try:
        valor = input('Notas: ').split()
        for i in valor:
            lista.append(int(i))
            soma = soma + int(i)
    except:
        continue
    break

media = sum(lista) / len(lista)

print(f'Quantidade de Termos: {len(lista)}')
print(f'Média: {media:.2f}')
print(f'Soma: {soma:.2f}')

