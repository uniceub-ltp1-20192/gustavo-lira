import sys

suspects = ['UESLLEI', 'MARIA CÁLICE', 'ROBERTA CORDA', 'RENATO FACA', 'ROBERTO ARMA']

def bye():
    sys.exit('Fim do Programa')

def askName():
    if not name in suspects:
        print('Tá errado!!! ')
        bye()
    print('Nome validado!')

def validVogals(name):
    contagem = name.count('A') + name.count('E') + name.count('I') + name.count('O') + name.count('U') + name.count('Á')
    print(f'> {name} possui {contagem} vogais!')
    if not contagem >= 3:
        print(f'> {name} possui menos de 3 vogais! Não é o Assassino!')
        bye()
    oustate = name.count('O') + name.count('U')
    if oustate >= 1:
        print(f'> {name} possui "U" ou "O"')
        print('INOCENTE!')
        bye()

def gun(name):
    splitted = name.split()
    second = splitted[1]
    white = ['CÁLICE', 'CALICE', 'FACA', 'CORDA']
    if second in white:
        print('Cálice é uma arma branca!')
        print(f'{name} é a Assassina!!!')
    bye()


print('''   BEM-VINDO DETETIVE!
Descubra quem matou o hóspede do quarto 101!
Seus suspeitos: Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
Regras:
1°) Se o nome do suspeito contém 3 vogais ou mais e nenhuma dessas vogais é "U" ou "O", ele pode ser o assassino.
2°) Se o suspeito for do sexo feminino, só será assassina se tiver utilizado uma arma branca.
3°) Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel, às 00:30 para ser considerado o assassino.''')

name = str(input('Digite o nome do suspeito: ')).upper().strip()
askName()
validVogals(name)
gun(name)
bye()