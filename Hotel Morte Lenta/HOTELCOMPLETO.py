from time import sleep
import sys

suspects = ['UESLLEI', 'MARIA CÁLICE', 'ROBERTA CORDA', 'RENATO FACA', 'ROBERTO ARMA']

def askName():
    if not name in suspects:
        print(f'O nome "{name}" não está na lista de suspeitos! ')
        choice = input('Deseja continuar mesmo assim? [S/N]: ').upper().strip()
        if choice == 'N':
            bye()
    jump(), print('Nome validado!'), jump(), sleep(1)

def validVogals(name):
    splitted = name.split()
    contagem = splitted[0].count('A') + splitted[0].count('E') + splitted[0].count('I') + splitted[0].count('O') + splitted[0].count('U') + splitted[0].count('Á')
    print(f'> {splitted[0]} possui {contagem} vogais!')
    sleep(1)
    if not contagem >= 3:
        print(f'> O 1° nome de {name} possui menos de 3 vogais! Não é o Assassino!')
        bye()
    oustate = name.count('O') + name.count('U')
    if oustate >= 1:
        jump()
        print(f'> {splitted[0]} possui "U" ou "O"')
        sleep(1)
        print('INOCENTE!')
        jump()
        bye()

def bioGender(name):
    if not name in suspects:
        jump()
        who = input('Homem ou Mulher? [H/M]: ').upper().strip()
        if who == 'H':
            timeset(name)
        elif who == 'M':
            weapon(name)
    print('Suspeita')
    jump()

def weapon(name):
    gun = 0
    askMurder(gun, 'Era uma Arma Branca? [S/N]: ', 'S')

def timeset(name):
    local = 0
    askMurder(local, 'Onde estava às 00:30?: ', 'SAGUÃO')

def askMurder(var, msg, msg2):
    var = input(msg).upper().strip()
    if var == msg2:
        welcome()
        print('!!! ASSASSINA !!!')
        bye()
    else:
        print('Inocente')
        bye()

def jump():
    print('')

def welcome():
    print('-' * 60)

def bye():
    sys.exit('Fim do Programa')


#Programa Principal

welcome()
print('''   BEM-VINDO DETETIVE!

Descubra quem matou o hóspede do quarto 101!
Seus suspeitos: Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.

Regras:
    1°) Se o nome do suspeito contém 3 vogais ou mais e nenhuma dessas vogais é "U" ou "O", ele pode ser o assassino.
	2°) Se o suspeito for do sexo feminino, só será assassina se tiver utilizado uma arma branca.
	3°) Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel, às 00:30 para ser considerado o assassino.''')

jump()
welcome()
name = str(input('Digite o nome do suspeito: ')).upper().strip()
askName()
validVogals(name)
bioGender(name)
weapon(name)
timeset(name)
