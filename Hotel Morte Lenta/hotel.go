package main
import (
"fmt"
"strings")

/*
Descubra quem matou o hóspede do quarto 101!
Seus suspeitos: Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
Regras:
1°) Se o nome do suspeito contém 3 vogais ou mais e nenhuma dessas vogais é "U" ou "O", ele pode ser o assassino.
2°) Se o suspeito for do sexo feminino, só será assassina se tiver utilizado uma arma branca.
3°) Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel, às 00:30 para ser considerado o assassino.
*/

func main() {

  name := askName() // Solicitar nome ao usuário
  tv := threeVogals(name) //Verifica se tem 3 vogais ou mais
  if ! tv{
	fmt.Println("Não contém 3 vogais. Inocente!")
	return
  }

  ou := ouVogals(name) //Verifica O ou U
  if ! ou{
	fmt.Println("Contém 'o' ou 'u'. Inocente!")
	return
  }

  biogender := getBioGender(name) //Descobre o gênero biológico
  weapon := ""
  clock := ""

  if biogender == "M"{
	fmt.Print("Que horas você estava no saguão? [hh:mm]: ")
	fmt.Scanln(&clock) //chama a variável horário
  }

  if biogender == "F"{
	fmt.Print("Era uma arma branca? [S/N]: ")
	fmt.Scanln(&weapon) //chama a variável arma
  }

  vw := validWoman(biogender, weapon)
  vm := validMan(biogender, clock)

  if ! (vw || vm){
	fmt.Println("Inocente.")
	return
  }

fmt.Println("ASSSSSSAAAAAAASSSSSIIIIIIINOOOOO!!!!!!!!!!!!!!!!!!!!!!!!!!")
}


func askName() string {
	var name string // declara a var input
	fmt.Print("Nome: ") // apresenta a string Nome
  	fmt.Scanln(&name) // lê o que o usuário digitou
  	return name
}

func threeVogals(nome string) bool{
	vogalsCount := 0
	vogais := "AEIÁOUáaeiou"
  	for _,n := range nome{
	for _,v := range vogais{
	if n == v{
		vogalsCount++
			}
		}
	}
  return vogalsCount >= 3
}

func ouVogals(nome string) bool{
  return !strings.Contains(strings.ToLower(nome),"o") && !strings.Contains(strings.ToLower(nome),"u")
}

func getBioGender(nome string) string{
  var sex string 
  fmt.Print("Sexo Biológico [M/F]: ")
  fmt.Scanln(&sex) 
  return sex
}

func validWoman(biogender string, weapon string) bool{
  return biogender == "F" && weapon == "S"
}

func validMan(biogender string, clock string) bool{
  return biogender == "M" && clock == "00:30"
}
