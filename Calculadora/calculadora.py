def askNumber(msg):
    a = int(input(f'Digite o {msg}° número: '))
    return a

def askOperator():
    b = input('Digite a Operação: ')
    return b

def validOperator(b):
    operatorsList = ['+', '-', '/', '*']
    if b not in operatorsList:
        return False
    return True


while True:
    num1 = askNumber('1')
    break

while True:
    op = askOperator()
    if validOperator(op) == True:
        break

while True:
    num2 = askNumber('2')
    if validNumber(num2) == True:
        break


print(num1, op, num2)
