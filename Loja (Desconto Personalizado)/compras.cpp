#include <iostream>
using namespace std;

int main() {
  
  float discount, price;
  char continuar;
  float total = 0;
  float totaldiscount = 0;

  while(continuar != 'N'){
  cout << "Insira o preço: R$";
  cin >> price;
  cout << "Desconto aplicado: ";
  cin >> discount;
  cout << "Quer continuar? [S/N]: ";
  cin >> continuar;
  cout << endl;

  total += price;
  totaldiscount += price - (discount / 100) * price;
  }
  
  cout << "O valor total foi de R$"<< total << endl;
  cout << "O valor com desconto foi de R$" << totaldiscount << endl;  
  

  return 0;
}