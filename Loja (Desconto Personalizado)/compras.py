#loja -> todos os produtos estão em promoção, cada um tem sua promoção específica.
#usuário = atendente da loja

#Qual valor?:
#Porcentagem da promoção?:

# Output: A soma dos valores sem a promoção e a soma dos produtos com a promoção

def askPrice():
  return float(input(f'Insira o valor do {cont+1}° produto: R$'))

def askDiscount():
  return int(input('Insira o desconto [0-100]: '))

def addCart():
    if cont == 1:
        print('1 produto adicionado ao carrinho')
    else:
        print(f'{cont} produtos adicionados ao carrinho')

total = 0
totaldiscount = 0
cont = 0
print('Promoção de Produtos. Digite 0 no valor para parar.')

while True:

  #tratamento de erros no preço do produto

  try: price = askPrice()
  except: continue
  if price == 0: break
  cont += 1

  #tratamento de erros no desconto (se não estiver entre 0 e 100 repete a pergunta até encontrar um válido)

  while True:
    try:
      discount = askDiscount()
      addCart()
      print('-'*30)
      if discount not in range(0, 101): continue
    except: continue
    else: break

  #cálculo do valor total com/sem desconto

  total += price
  totaldiscount += price - (discount / 100) * price

print('''
Total de {} produtos.
Você pagaria: R${:.2f}
Você vai pagar: R${:.2f}
O valor do desconto foi de R${:.2f}!'''.format(cont, total, totaldiscount, total-totaldiscount))
