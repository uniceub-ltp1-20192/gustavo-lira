import time

class Menu:
    @staticmethod
    def iniciarMenu():
        opMenu = ''
        print('\n', '>'*10, 'PROJETO FINAL', '<'*10)
        while opMenu != '0':
            opMenu = input('[1] Consultar\n[2] Inserir\n[3] Alterar\n[4] Deletar\n[0] Sair\n>>> ')
            if opMenu == '1': Menu.menuConsultar()
            elif opMenu == '2': Menu.menuAlterar()
            elif opMenu == '3': print('Entrei em Alterar\n')
            elif opMenu == '4': print('Entrei em Deletar\n')
            elif opMenu == '0': print('\n')
            else: print('Digite uma opção válida\n')
            time.sleep(1)

    @staticmethod
    def menuConsultar():
        consultar = input('[1] Consultar por identidade\n[2] Consultar por propriedade\n>>>')
        if consultar == '1': print('ENTREI EM CONSULTAR IDENTIDADE')
        elif consultar == '2': print('ENTREI EM CONSULTAR PROPRIEDADE') 

    @staticmethod
    def menuAlterar():
        alterar = input('[1] Alterar Valor\n[2] Alterar Operador\n>>>')
        if alterar == '1': print('Altera valor')
        if alterar == '2': print('Altera operador')


Menu.iniciarMenu()
